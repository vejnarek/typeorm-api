# Nastaveni (ENV PROMENNE)

## Povinne

| Nazev       | Popis                        | Default |
| ----------- | ---------------------------- | ------- |
| DB_HOST     | IP adresa pro pripojeni k DB |         |
| DB_NAME     | Nazev databaze               |         |
| DB_USER     | Prihlasovaci jmeno k DB      |         |
| DB_PASSWORD | Prihlasovaci heslo k DB      |         |

## Nepovinne

| Nazev                | Popis                              | Default |
| -------------------- | ---------------------------------- | ------- |
| DB_LOGGING           | logovani z databaze                | false   |
| MAX_CHARACTERS_COUNT | nastaveni maximalniho poctu postav | 4       |
