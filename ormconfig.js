const dbConnectionConfig = require('./src/config').database
module.exports = {
  type: 'postgres',
  port: 5432,
  entities: ['src/postgres/entities/*.js'],
  synchronize: process.env.NODE_ENV === 'development',
  ...dbConnectionConfig
}
