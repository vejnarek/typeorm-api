module.exports = {
  database: {
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    logging: process.env.DB_LOGGING === 'true'
  },
  accessTokenExpiration: process.env.ACCESS_TOKEN_EXPIRATION || '24h',
  classes: ['WARRIOR', 'ARCHER'],
  maxCharactersCount: parseInt(process.env.MAX_CHARACTERS_COUNT, 10) || 4
}
