const EntitySchema = require('typeorm').EntitySchema
// statistiky
// inventar
module.exports = new EntitySchema({
  name: 'Character',
  tableName: 'characters',
  columns: {
    id: {
      primary: true,
      type: 'uuid',
      generated: 'uuid'
    },
    name: {
      type: 'varchar',
      unique: true,
      nullable: false
    },
    class: {
      type: 'varchar',
      nullable: false
    },
    level: {
      type: 'int',
      nullable: false,
      default: 1
    }
  },
  relations: {
    account: {
      target: 'Account',
      type: 'many-to-one',
      joinColumn: true
    }
  }
})
