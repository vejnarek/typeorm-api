const EntitySchema = require('typeorm').EntitySchema

module.exports = new EntitySchema({
  name: 'Account',
  tableName: 'accounts',
  columns: {
    id: {
      primary: true,
      type: 'uuid',
      generated: 'uuid'
    },
    email: {
      type: 'varchar',
      unique: true,
      nullable: false
    },
    username: {
      type: 'varchar',
      nullable: false
    },
    password: {
      type: 'varchar',
      select: false,
      nullable: false
    },
    activated: {
      type: 'boolean',
      default: false,
      select: false
    },
    role: {
      type: 'int',
      default: 1,
      select: false
    }
  },
  relations: {
    characters: {
      target: 'Character',
      type: 'one-to-many',
      inverseSide: 'account',
      cascade: true
    }
  }
})
