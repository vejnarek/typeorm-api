const { createConnection } = require('typeorm')
const { logger } = require('app-utils')

module.exports = () => {
  createConnection()
    .then(() => {
      logger.info(`DATABASE CONNECTION – SUCCESS`)
    })
    .catch((err) => {
      logger.error(`DATABASE CONNECTION ERROR – ${err}`)
    })
}
