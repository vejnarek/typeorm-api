const { Router } = require('express')
const { status } = require('./controller')
const router = Router()
const baseUrl = '/status'

router.get(baseUrl, status)

module.exports = router
