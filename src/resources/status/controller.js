const { version } = require('../../../package.json')
module.exports.status = (req, res) => {
  res.status(200).json({ status: 'Running', version })
}
