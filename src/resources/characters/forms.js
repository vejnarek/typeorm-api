const { classes } = require('../../config')
const { param, body } = require('express-validator')
const { helper } = require('app-utils')

module.exports.charactersById = [
  param('accountId').exists().withMessage('missing_id').not().isEmpty().matches(helper.uuidRegex).withMessage('invalid_format')
]

module.exports.create = [
  param('accountId')
    .exists()
    .withMessage('missing_accountId')
    .not()
    .isEmpty()
    .matches(helper.uuidRegex)
    .withMessage('invalid_accountId_format'),
  body('name').exists().withMessage('missing_name').not().isEmpty().isLength({ min: 3, max: 24 }).withMessage('invalid_name_format'),
  body('class').exists().withMessage('missing_class').not().isEmpty().isIn(classes).withMessage('unsupported_class')
]
