const { Router } = require('express')
const controller = require('./controller')
const forms = require('./forms')
const { checkErrors, policy } = require('app-utils')

const router = Router()
const baseUrl = '/character'

router.get(`${baseUrl}s/classes`, policy.isSignedIn, controller.classes)

router.get(`${baseUrl}s/:accountId`, forms.charactersById, policy.isSignedIn, controller.charactersById)

router.post(`${baseUrl}/create/:accountId`, forms.create, checkErrors, policy.isSignedIn, controller.create)

module.exports = router
