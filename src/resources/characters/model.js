const { getRepository, createQueryBuilder } = require('typeorm')

module.exports.isNameUnique = async (name) => {
  const result = await getRepository('Character').find({ name })
  return result.length ? false : true
}

module.exports.charactersById = async (id) => {
  return await getRepository('Account').findOne({ where: { id }, relations: ['characters'] })
  // await createQueryBuilder().
}

module.exports.create = async (account, name, characterClass) => {
  const character = {
    name,
    class: characterClass
  }
  await getRepository('Character').save(character)

  account.characters.push(character)
  await getRepository('Account').save(account)
}
