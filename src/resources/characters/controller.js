const config = require('../../config')
const characterModel = require('./model')
const accountModel = require('../accounts/model')
const { logger } = require('app-utils')

module.exports.charactersById = async (req, res, next) => {
  try {
    logger.debug('CHARACTERS – START')

    const account = await accountModel.accountById(req.params.accountId)
    if (!account) {
      logger.debug('CHARACTES – ACCOUNT NOT FOUND')
      return res.status(400).json({ error: 'account_not_found' })
    }

    logger.debug('CHARACTERS – SUCCESS')
    return res.json(account.characters)
  } catch (err) {
    logger.debug('CHARACTERS – ERROR')
    next(err)
  }
}

module.exports.classes = async (req, res, next) => {
  res.json(config.classes)
}

module.exports.create = async (req, res, next) => {
  try {
    logger.debug('CHARACTER CREATION – START')

    // Kontrola na unikatni jmeno charakteru
    if (!(await characterModel.isNameUnique(req.body.name))) {
      logger.debug('CHARACTER CREATION – NAME IS ALREADY USED')
      return res.status(400).json({ error: 'name_is_already_used' })
    }

    // Kontrola a vyber uctu
    const account = await accountModel.accountById(req.params.accountId)
    if (!account) {
      logger.debug('CHARACTER CREATION – ACCOUNT NOT FOUND')
      return res.status(400).json({ error: 'account_not_found' })
    }

    if (account.characters.length >= config.maxCharactersCount) {
      logger.debug('CHARACTER CREATION – MAX CHARACTERS COUNT')
      return res.status(400).json({ error: 'max_characters_count' })
    }
    // Pridani charakteru
    await characterModel.create(account, req.body.name, req.body.class)

    logger.debug('CHARACTER CREATION – SUCCESS')
    return res.sendStatus(200)
  } catch (err) {
    logger.debug('CHARACTER CREATION – ERROR')
    next(err)
  }
}
