const { param } = require('express-validator')
const { helper } = require('app-utils')

module.exports.accountById = [
  param('id').exists().withMessage('missing_id').not().isEmpty().matches(helper.uuidRegex).withMessage('invalid_format')
]

module.exports.deleteAccount = [
  param('loggedAccountId')
    .exists()
    .withMessage('missing_logged_account_id')
    .not()
    .isEmpty()
    .matches(helper.uuidRegex)
    .withMessage('invalid_format'),
  param('accountToDeleteId')
    .exists()
    .withMessage('missing_account_to_delete_id')
    .not()
    .isEmpty()
    .matches(helper.uuidRegex)
    .withMessage('invalid_format')
]
