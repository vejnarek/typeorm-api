const { getRepository } = require('typeorm')

module.exports.accounts = async () => {
  return await getRepository('Account').find()
}

module.exports.accountById = async (id) => {
  const account = await getRepository('Account').findOne({ where: { id }, relations: ['characters'] })
  return account ? account : false
}
module.exports.isAdmin = async (id) => {
  const account = await getRepository('Account').findOne({ select: ['role'], where: { id } })
  return account?.role === 10
}
module.exports.deleteAccount = async (id) => {
  const account = await this.accountById(id)
  return account ? await getRepository('Account').remove(account) : false
}
