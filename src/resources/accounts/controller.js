const accountModel = require('./model')
const { logger } = require('app-utils')

module.exports.accounts = async (req, res, next) => {
  try {
    logger.debug('ACCOUNTS – START')
    const result = await accountModel.accounts()
    logger.debug('ACCOUNTS – SUCCESS')
    return res.status(200).json(result)
  } catch (err) {
    logger.debug('ACCOUNTS – ERROR')
    next(err)
  }
}

module.exports.accountById = async (req, res, next) => {
  try {
    logger.debug('ACCOUNT BY ID – START')
    const account = await accountModel.accountById(req.params.id)
    if (!account) {
      logger.debug('ACCOUNT BY ID – ACCOUNT NOT FOUND')
      return res.status(400).json({ error: 'account_not_found' })
    }

    logger.debug('ACCOUNT BY ID – SUCCESS')
    return res.status(200).json(account)
  } catch (err) {
    logger.debug('ACCOUNT BY ID – ERROR')
    next(err)
  }
}
module.exports.deleteAccount = async (req, res, next) => {
  try {
    logger.debug('DELETE ACCOUNT – START')
    const admin = await accountModel.isAdmin(req.params.loggedAccountId)
    const accountDeletion = req.params.loggedAccountId === req.params.accountToDeleteId
    if (!admin && !accountDeletion) {
      logger.debug('DELETE ACCOUNT – NO RIGHTS')
      return res.status(400).json({ error: 'account_cannot_be_deleted' })
    }
    const account = await accountModel.deleteAccount(req.params.accountToDeleteId)
    if (!account) {
      logger.debug('DELETE ACCOUNT – ACCOUNT NOT FOUND')
      return res.status(400).json({ error: 'account_not_found' })
    }
    logger.debug('DELETE ACCOUNT – SUCCESS')
    return res.sendStatus(200)
  } catch (err) {
    logger.debug('DELETE ACCOUNT – ERROR')
    next(err)
  }
}
