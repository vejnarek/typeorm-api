const { Router } = require('express')
const { policy, checkErrors } = require('app-utils')
const controller = require('./controller')
const forms = require('./forms')

const router = Router()
const baseUrl = '/account'

router.get(`${baseUrl}s`, policy.isSignedIn, controller.accounts)

router.get(`${baseUrl}/:id`, forms.accountById, checkErrors, policy.isSignedIn, controller.accountById)

router.delete(
  `${baseUrl}/:loggedAccountId/:accountToDeleteId`,
  forms.deleteAccount,
  checkErrors,
  policy.isSignedIn,
  controller.deleteAccount
)

module.exports = router
