const { getRepository } = require('typeorm')
const bcrypt = require('bcrypt')
const { logger } = require('app-utils')

module.exports.isEmailUnique = async (email) => {
  const result = await getRepository('Account').find({ email })
  return result.length ? false : true
}

module.exports.register = async (username, password, email) => {
  const account = {
    email,
    username
  }
  logger.debug('REGISTRATION – PASSWORD ENCRYPTION')
  await bcrypt
    .hash(password, 10)
    .then((hash) => {
      account.password = hash
      logger.debug('REGISTRATION – PASSWORD ENCRYPTION SUCCESS')
    })
    .catch(() => {
      logger.debug('REGISTRATION – PASSWORD ENCRYPTION ERROR')
    })
  await getRepository('Account').save(account)
}

module.exports.login = async (passwd, email) => {
  const account = await getRepository('Account').findOne({
    select: ['id', 'username', 'password', 'activated', 'role'],
    relations: ['characters'],
    where: { email }
  })
  let isPasswordMatching
  if (!account) {
    logger.debug('LOGIN – ACCOUNT NOT FOUND')
    return false
  }
  logger.debug('LOGIN – PASSWORD DECRYPTION')
  await bcrypt
    .compare(passwd, account.password)
    .then((result) => {
      isPasswordMatching = result
      logger.debug(`LOGIN – PASSWORD DECRYPTION SUCCESS – MATCHING: ${isPasswordMatching.toString().toUpperCase()}`)
    })
    .catch(() => {
      logger.debug('LOGIN – PASSWORD DECRYPTION ERROR')
    })
  const { password, ...accountWithoutPassword } = account
  return isPasswordMatching ? accountWithoutPassword : false
}
