const authModel = require('./model')
const { accessTokenExpiration } = require('../../config')
const { policy } = require('app-utils')
const { logger } = require('app-utils')

module.exports.register = async (req, res, next) => {
  try {
    logger.debug('REGISTRATION – START')
    if (!(await authModel.isEmailUnique(req.body.email))) {
      logger.debug('REGISTRATION – EMAIL IS ALREADY USED')
      return res.status(400).json({ error: 'email_is_already_used' })
    }
    await authModel.register(req.body.username, req.body.password, req.body.email)
    logger.debug('REGISTRATION – SUCCESS')
    return res.sendStatus(200)
  } catch (err) {
    logger.debug('REGISTRATION – ERROR')
    next(err)
  }
}

module.exports.login = async (req, res, next) => {
  try {
    logger.debug('LOGIN – START')
    const account = await authModel.login(req.body.password, req.body.email)
    if (!account) {
      logger.debug('LOGIN – WRONG CREDENTIALS')
      return res.status(400).json({ error: 'wrong_credentials' })
    }
    logger.debug('LOGIN – CREATE TOKEN')
    const accessToken = await policy.createToken({ username: account.username }, accessTokenExpiration)
    logger.debug('LOGIN – SUCCESS')
    return res.status(200).json({ accessToken, account })
  } catch (err) {
    logger.debug('LOGIN – ERROR')
    next(err)
  }
}

module.exports.logout = async (req, res, next) => {
  try {
    logger.debug('LOGOUT – START')
    logger.debug('LOGOUT – SUCCESS')
    res.json(204)
  } catch (err) {
    logger.debug('LOGOUT – ERROR')
    next(err)
  }
}
