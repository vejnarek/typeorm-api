const { Router } = require('express')
const controller = require('./controller')
const forms = require('./forms')
const { checkErrors } = require('app-utils')

const router = Router()
const baseUrl = '/authentication'

router.post(`${baseUrl}/register`, forms.register, checkErrors, controller.register)

router.post(`${baseUrl}/login`, forms.login, checkErrors, controller.login)

router.delete(baseUrl, controller.logout)

module.exports = router
