const { body } = require('express-validator')

module.exports.register = [
  body('email').exists().withMessage('missing_email').not().isEmpty().isEmail().withMessage('invalid_email_format'),
  body('username')
    .exists()
    .withMessage('missing_username')
    .not()
    .isEmpty()
    .isLength({ min: 3, max: 24 })
    .withMessage('invalid_username_format'),
  body('password')
    .exists()
    .withMessage('missing_password')
    .not()
    .isEmpty()
    .isLength({ min: 8, max: 64 })
    .withMessage('invalid_password_format')
]

module.exports.login = [
  body('email').exists().withMessage('missing_email').not().isEmpty().isEmail().withMessage('invalid_email_format'),
  body('password')
    .exists()
    .withMessage('missing_password')
    .not()
    .isEmpty()
    .isLength({ min: 8, max: 64 })
    .withMessage('invalid_password_format')
]
