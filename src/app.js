const { urlencoded, json } = require('express')
const cookieParser = require('cookie-parser')
const cors = require('cors')
const dotenv = require('dotenv')
const path = require('path')
const { logger, requestLogger, resources } = require('app-utils')

module.exports = async (app) => {
  // Nastavení dotenv
  dotenv.config({ path: '.env' })

  // Nastavení základních middlewaru
  app.use(cookieParser())
  app.use(json())
  app.use(urlencoded({ extended: false }))
  app.use(
    cors({
      origin: true,
      credentials: true,
      allowedHeaders: ['Origin', 'Content-Type', 'Authorization']
    })
  )

  // Nastavení request loggeru (z privátní knihovny k logování požadavků na server)
  app.use(requestLogger)

  // Nastavení všech rout ze složky resources a přidání do express aplikace
  resources(app, logger, '/api', path.join(`${__dirname}/resources`, '.'))

  // Middleware, ktery nastavi 404 response kdyz prijdes na neexistujici routu
  app.use('*', (req, res) => {
    res.status(404).json({ message: 'Route not found.' })
  })

  return app
}
