const express = require('express')
const application = require('./app')
const version = require('../package.json').version
const dbConnection = require('./postgres/db-connection')
const { logger } = require('app-utils')

const main = async () => {
  const app = express()
  await application(app)

  // Nastavení portu aplikace (hlavně pro vývoj)
  const port = parseInt(process.env.PORT, 10) || 2000
  app.set('port', port)

  app.listen(port, () => {
    logger.info(`SERVER STARTED – [VERSION]: ${version} [PORT]: ${port}`)
    logger.debug(`DEBUG MODE – ACTIVATED`)
  })

  dbConnection()
}

module.exports = main().catch((err) => {
  logger.error('APPLICATION ERROR –', err)
  process.exit(1)
})
